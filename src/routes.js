import React from "react";
import "./routes.css";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Fonctions from "./pages/fonctions/fonctions"
import Login from "./pages/login/login";
import Details from "./pages/detail/details";
import Detail from "./pages/detail/details";
import Normes from "./pages/normes/normes";
import Questions from "./pages/question/questions";
import Audit from "./pages/audit/audit";
import ErrorPage from "./pages/errorPage";
import Assessment from "./pages/assessment/assessment";
import G_archive from "./pages/g_archive/g_archive";
import G_user from "./pages/g_user/g_user";
import Ajout_user from "./pages/ajout_user/ajout_user";
import Rapport from "./pages/rapport/rapport";
import List_user from "./pages/list_user/list_user";
import Ajout_audit from "./pages/ajout_audit/ajout_audit";
import Supp_audit from "./pages/supp_audit/supp_audit";
import Box from "@mui/material/Box";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import MenuIcon from "@mui/icons-material/Menu";
import Typography from "@mui/material/Typography";

export default function AppRoutes() {
  return (

      <Router>
        <Box p={2} sx={{flexGrow: 1}}>
          <AppBar position="static">
            <Toolbar>
              <MenuIcon/>
              <Typography
                  pl={2}
                  variant="h6"
                  noWrap
                  component="a"
                  href="/fonctions"
                  sx={{
                    mr: 2,
                    display: {xs: 'none', md: 'flex'},
                    fontWeight: 700,
                    letterSpacing: '.3rem',
                    color: 'inherit',
                    textDecoration: 'none',
                  }}
              >
                Menu
              </Typography>
            </Toolbar>
          </AppBar>
        </Box>

        <div className="row">
          <Routes>
            <Route path="/" element={<Login/>}/>
            <Route path="/fonctions" element={<Fonctions/>}/>
            <Route path="/details" element={<Details/>}/>
            <Route path="/normes" element={<Normes/>}/>
            <Route path="/questions" element={<Questions/>}/>
            <Route path="/errorpage" element={<ErrorPage/>}/>
            <Route path="/audit" element={<Audit/>}/>
            <Route path="/assessment" element={<Assessment/>}/>
            <Route path="/detail" element={<Detail/>}/>
            <Route path="/g_archive" element={<G_archive/>}/>
            <Route path="/g_user" element={<G_user/>}/>
            <Route path="/ajout_user" element={<Ajout_user/>}/>
            <Route path="/rapport" element={<Rapport/>}/>
            <Route path="/list_user" element={<List_user/>}/>
            <Route path="/ajout_audit" element={<Ajout_audit/>}/>
            <Route path="/supp_audit" element={<Supp_audit/>}/>
          </Routes>
        </div>
      </Router>
  );
}
