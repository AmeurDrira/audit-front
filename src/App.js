import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import AppRoutes from './routes';
export default function App() {
  return (
    <AppRoutes />
  );
}
