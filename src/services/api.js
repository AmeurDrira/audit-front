import axios from "axios";
import { URL } from '../constants/utils'
const api = axios.create({
    baseURL: URL,
    headers: {
        'X-USER': '001',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    withCredentials: true,
    }
});

export default api;
