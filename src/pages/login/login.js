import React, {Component} from "react";
import "./login.css";
import api from "../../services/api";
import { Link } from "react-router-dom";
export default class Login extends Component {
  state = {
    log: 'string',
    mdp:'string',
  }

  handleChange = event => {
    this.setState({ log: event.target.value });
    this.setState({ mdp: event.target.value});
  }
  
  handleSubmit = event => {
    console.log("erreur");
    event.preventDefault();

    const user = {
      log: this.state.log,
      mdp: this.state.mdp,
    };

    api.post(`/api/utilisateur/check-user`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {

      return (
        <div className="Login">
          <form onSubmit={this.handleSubmit}>
            
              <input type="text" placeholder="LOGIN" className="login" log="login" onChange={this.handleChange} />
           
           <br/>
              <input type="text" placeholder="MOT DE PASSE" className="pwd" mdp="motdepasse" onChange={this.handleChange} />
              <br/>
            
        
              <Link to="/fonctions"> 
            <button type="submit" className="btn btn-primary sign-in">Submit</button>
             </Link>
            </form>
        </div>
      );
      }
  
  
}
