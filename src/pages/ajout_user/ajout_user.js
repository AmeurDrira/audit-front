import React, {Component} from "react";
import "./ajout_user.css";
import api from "../../services/api";
import {url_utilisateurs} from "../../constants/utils";
import { Link } from "react-router-dom";
export default class AjoutUser extends Component {
  // state = {
  //   user : {
  //     gaia: "",
  //     prenom: "",
  //     nom: "",
  //     motdepasse: "",
  //   }
  // }
  // constructor(props) {
  //   super(props)
  //
  // }
  // onChange = (event) => {
  //   this.setState({
  //     [event.target.name] : event.target.value
  //   });
  //   // console.log(this.state.nom)
  // }
  // handleInputChange = (event) => {
  //   const {name, value} = event.target;
  //   this.setState({
  //     [name]: value
  //   });
  // };

  state = {
      gaia: "",
      prenom: "",
      nom: "",
      motdepasse: "",
      profil:""

  }

  onChange = (event) => {
    console.log(event.target.value);
    console.log(event.target.name);
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  onSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
    api.post(url_utilisateurs, this.state).then(res => {
      console.log(res);
      console.log(res.data);
    })
   

  }

  render() {
    return (
        // <Box
        //     style={{"backgroundColor": "white"}}
        //     component="form"
        //     sx={{
        //       '& > :not(style)': {m: 1, width: '25ch'},
        //     }}
        //     noValidate
        //     autoComplete="off"
        // >
        //   <TextField
        //       name="name" // Add this
        //       label="Name"
        //       value={this.state.user.nom}
        //       margin="normal"
        //       variant="outlined"
        //   />
        //
        //   <TextField name="prenom" label="prenom"
        //              variant="outlined" />
        //   <TextField label="gaia" name="gaia" variant="outlined"/>
        //
        //   <button type="submit" className="submit-btn btn btn-primary"
        //           onClick={this.saveOrUpdateEmployee}>
        //     + Ajouter
        //   </button>
        // </Box>
        <div>
        <div className="Options">
      
      
      
        <Link to="/ajout_user">
            <button type="button" className="ajout">
            + Ajouter 
            </button>
        </Link>
        
       
        
        <Link to="/list_user">
            <button type="button" className="supp">
            - Supprimer
            </button>
        </Link>
        
        
        
        <Link to="/list_user">
            <button type="button" className="modif">
           * Modifier
            </button>
        </Link>
        
    
    </div>
        <div>
          <div className="card my-3 m-5">
            <div className="card-header">Ajouter un utilisateur</div>
            <div className="card-body">

              <form onSubmit={this.onSubmit}>

                <div className="form-group">
                  <label className="form-label" htmlFor='nom'>Nom</label>
                  <input type="text" className="form-control form-control-lg"
                         name="nom"
                         onChange={this.onChange}
                         value={this.state.nom}
                  />
                </div>
                <div className="form-group mb-3">
                  <label className="form-label" htmlFor='ville'>Prenom</label>
                  <input type="text" className="form-control form-control-lg"
                         name="prenom"
                         onChange={this.onChange}
                         value={this.state.prenom}
                  />
                </div>

                <div className="form-group  py-3">
                  <label className="form-label" htmlFor='Login'>Login</label>
                  <input type="text" className="form-control form-control-lg"
                         name="gaia"
                         onChange={this.onChange}
                         value={this.state.gaia}
                  />
                </div>

                <div className="form-group mb-3">
                  <label className="form-label" htmlFor='ville'>Mot de
                    passe </label>
                  <input type="text" className="form-control form-control-lg"
                         name="motdepasse"
                         onChange={this.onChange}
                         value={this.state.motdepasse}
                  />
                </div>

                <div className="form-group mb-3">
                  <label className="form-label" htmlFor='ville'>Role </label>
                    <select name="profil" className="form-control form-control-lg" onChange={this.onChange} value={this.state.profil} >
                      <option>choisissez vous</option>
                      <option value="AUDITEUR" >AUDITEUR</option>
                      <option value="ADMIN">ADMIN</option>
                    </select>
                </div>
                <div className="form-group mb-3">
                  <button className="btn btn-primary btn-block fixed-right"
                          type="submit">Créez utilisateur
                  </button>
                  </div>
              </form>
            </div>
          </div>
        </div>
        </div>
    )

    {/*<div className="title">Nom</div>*/
    }
    {/*<input type="text" name="nom" className="name"*/
    }
    {/*       value={this.state.nom}/>*/
    }

    //
    //

    //            value={this.state.prenom}/>
    //     <div className="title">Login</div>
    //     <input type="text" className="login" name="login"
    //            value={this.state.gaia}/>
    //     <div className="title">Mot De Passe</div>
    //     <input type="text" className="pwd" name="motdepasse"
    //            value={this.state.motdepasse}/>
    //
    //     <div className="title">Profil</div>
    //
    //     <Select
    //         value={this.state.profile}
    //         onChange={this.handleInputChange}
    //         displayEmpty
    //         inputProps={{'aria-label': 'Without label'}}
    //     >
    //       <MenuItem value="">Auditeur</MenuItem>
    //       <MenuItem value="ADMIN">Administrateur</MenuItem>
    //     </Select>
    //
    //     <br/>

    //   </form>
    // </div>
    // );
  }
  }

