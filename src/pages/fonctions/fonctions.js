import React from "react";
import "./fonctions.css";
import {Link} from "react-router-dom";

export default function Fonctions() {

  return (

      <div className="App container">
        <div className="row company-logo">
          <div className="col-12 logo">
            <img src="logo.jpg" alt=""/>
          </div>
          <span className="col title">Security System Consulting</span>
        </div>

        <div className="Fonctions">
          <form className="fonctionnalités">
            <div><Link to="/details">
              <button type="button" className="audit">
                Générer un rapport
              </button>
            </Link>
            </div>
            <br/>
            <div>
              <Link to="/audit">
                <button type="button" className="G_audit">
                  Audits
                </button>
              </Link>
            </div>
            <br/>
            <div>
              <Link to="/g_archive">
                <button type="button" className="G_archive">
                  Archive
                </button>
              </Link>
            </div>
            <br/>
            <div>
              <Link to="/g_user">
                <button type="button" className="G_user">
                  Utilisateurs
                </button>
              </Link>
            </div>
          </form>
        </div>
      </div>
  );
}
