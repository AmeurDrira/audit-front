import React,{Component} from "react";
import "./list_user.css";
import api from "../../services/api";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Link } from "react-router-dom";
import {url_utilisateurs} from "../../constants/utils";

class List_user extends Component {
  constructor() {
    super();
    this.state = {
      name: "React",
      rows : [

      ]
    };
    
  }
  componentDidMount() {
    this.getAllUsers();
  }

  async  getAllUsers() {
    console.log("getAllUsers");
    const response = await api.get(url_utilisateurs);
    console.log(response.data);
    this.setState({ rows: response.data });
  }

render() {
  const { rows } = this.state;

      return (
        <div>
        <div className="Options">
      
      
      
        <Link to="/ajout_user">
            <button type="button" className="ajout">
            + Ajouter 
            </button>
        </Link>
        
       
        
        <Link to="/list_user">
            <button type="button" className="supp">
            - Supprimer
            </button>
        </Link>
        
        
        
        <Link to="/list_user">
            <button type="button" className="modif">
           * Modifier
            </button>
        </Link>
        </div>
        <div className="my-3 m-5">
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">gaia</TableCell>
            <TableCell align="left">profil</TableCell>
            <TableCell align="left">nom</TableCell>
            <TableCell align="left">prénom</TableCell>
            <TableCell align="left">mot de passe</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.gaia}
              </TableCell>
              <TableCell align="left"> {row.profil}</TableCell>
              <TableCell align="left">{row.nom}</TableCell>
              <TableCell align="left">{row.prenom}</TableCell>
              <TableCell align="left">{row.motdepasse}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
    </div>
  );
}}
export default List_user;
