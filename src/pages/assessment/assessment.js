import React from "react";
import "./assessment.css";
import { Container, Accordion, ListGroup } from "react-bootstrap";

export default function Assessment() {
  return (
    <div className="Assessment">
      <Container fluid>
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>PERSONAL COMPUTER SECURITY</Accordion.Header>
            <Accordion.Body className="secondList">
              <ListGroup as="ul">
                <ListGroup.Item as="li" className="audit">
                  MANAGEMENT
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  USERS ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER CONNECTION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  ANTIVIRUS PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  TRAINING PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  LAPTOP
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  BACK-UP
                </ListGroup.Item>
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>NETWORK ACCESS - WAN/LAN</Accordion.Header>
            <Accordion.Body>
              <ListGroup as="ul">
                <ListGroup.Item as="li" className="audit">
                  MANAGEMENT
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  USERS ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER CONNECTION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  ANTIVIRUS PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  TRAINING PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  LAPTOP
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  BACK-UP
                </ListGroup.Item>
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="2">
            <Accordion.Header>SERVER LOGICAL SECURITY</Accordion.Header>
            <Accordion.Body>
              <ListGroup as="ul">
                <ListGroup.Item as="li" className="audit">
                  MANAGEMENT
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  USERS ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER CONNECTION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  ANTIVIRUS PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  TRAINING PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  LAPTOP
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  BACK-UP
                </ListGroup.Item>
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="3">
            <Accordion.Header>PHYSICAL SECURITY</Accordion.Header>
            <Accordion.Body>
              <ListGroup as="ul">
                <ListGroup.Item as="li" className="audit">
                  MANAGEMENT
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  USERS ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER CONNECTION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  ANTIVIRUS PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  TRAINING PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  LAPTOP
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  BACK-UP
                </ListGroup.Item>
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="4">
            <Accordion.Header>BACK-UP COPIES</Accordion.Header>
            <Accordion.Body>
              <ListGroup as="ul">
                <ListGroup.Item as="li" className="audit">
                  MANAGEMENT
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  USERS ADMINISTRATION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  PERSONAL COMPUTER CONNECTION
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  ANTIVIRUS PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  TRAINING PROGRAM
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  LAPTOP
                </ListGroup.Item>
                <ListGroup.Item as="li" className="audit">
                  BACK-UP
                </ListGroup.Item>
              </ListGroup>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </Container>
    </div>
  );
}
