import React from "react";
import "./details.css";
import { Link } from "react-router-dom";
export default function Details() {
 
  return (
    <div className="Details">
      <form className="companyInfos">
        <div className="title">COMPANY</div>
        <input type="text" className="name"/>
        <div className="title">WEBSITE</div>
        <input type="url" className="website" />
        <div className="title">DATE</div>
        <input type="date" placeholder="DD-MM-YYYY" className="actual-date" />
        <br />
        <Link to="/Normes" >
          <button type="submit" className="submit-btn btn btn-primary">
            SUBMIT
          </button>
        </Link>
      </form>
    </div>
  );
}
