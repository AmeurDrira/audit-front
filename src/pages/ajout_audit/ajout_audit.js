import React from "react";
import "./ajout_audit.css";
import { Link } from "react-router-dom";

export default function Ajout_audit() {
 
  return (
    <div className="ajouter">
      <form className="ajout">
        <div className="title" >Intitulé</div>
        <input type="text" placeholder="audit de conformité ISO*" className="name"/>
        <div className="title">Date d'ajout</div>
        <input type="date" placeholder="DD-MM-YYYY" className="actual-date" />
        <div className="title">Responsable</div>
        <input type="text" className="resp" />
        <br />
          <button type="submit" className="submit-btn btn btn-primary">
           + Ajouter
          </button>
      </form>
    </div>
  );
}