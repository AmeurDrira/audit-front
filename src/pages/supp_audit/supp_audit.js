import React,{Component} from "react";
import "./supp_audit.css";
import api from "../../services/api";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Link } from "react-router-dom";

class Supp_audit extends Component {
  constructor() {
    super();
    this.state = {
      name: "React",
      rows : [

      ]
    };
    
  }
  componentDidMount() {
    this.getAllAudits();
  }

  async  getAllAudits() {
    console.log("getAllAudits");
    const response = await api.get('/api/audit');
    console.log(response.data);
    this.setState({ rows: response.data });
  }

render() {
  const { rows } = this.state;

      return (
        
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Intitulé</TableCell>
            <TableCell align="center">Date</TableCell>
            <TableCell align="center">Responsable</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.nom}
              </TableCell>
              <TableCell align="left"> {row.date}</TableCell>
              <TableCell align="left">{row.societe.nom}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    
    
  );
}}
export default Supp_audit;