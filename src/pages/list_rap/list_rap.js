import React,{Component} from "react";
import "./List_rap.css";
import api from "../../services/api";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Link } from "react-router-dom";

class Questions extends Component {
  constructor() {
    super();
    this.state = {
      name: "React",
      rows : [

      ]
    };
    
  }
  componentDidMount() {
    this.getAllQuestions();
  }

  async  getAllQuestions() {
    console.log("getAllQuestions");
    const response = await api.get('/api/question');
    console.log(response.data);
    this.setState({ rows: response.data });
  }

render() {
  const { rows } = this.state;

      return (
        <div>
          <div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Chapitre</TableCell>
            <TableCell align="center">Sous Chapitre</TableCell>
            <TableCell align="center">Question</TableCell>
            <TableCell align="center">Poids</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.souschapitre.chapitre.id} {row.souschapitre.chapitre.nom}
              </TableCell>
              <TableCell align="left"> {row.souschapitre.id} {row.souschapitre.nom}</TableCell>
              <TableCell align="left">{row.id} {row.nom}</TableCell>
              <TableCell align="left">{row.poids}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
    <div className="bouton d-flex justify-content-end">
    <Link to="/rapport">
    <button type="submit" className="submit-btn btn btn-primary">
            Enregistrer
    </button>
    </Link>
    </div>
    </div>
    
  );
}}
export default Questions;