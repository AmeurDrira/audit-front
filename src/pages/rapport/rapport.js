import React from 'react';
import { BarChart, Bar, Brush, XAxis, 
    YAxis, CartesianGrid} from 'recharts';


export default function Rapport(){


const data = [
  {name:'5', x:8},
  {name:'6', x:8},
  {name:'7', x:3},
  {name:'8', x:4},
  {name:'9', x:5},
  {name:'10', x:3},
  {name:'11', x:3},
  {name:'12', x:8},
  {name:'13', x:9},
  {name:'14', x:3},
  {name:'15', x:2},
  {name:'16', x:3},
  {name:'17', x:5},
  {name:'18', x:4},

];
  
return (
  <BarChart width={500} height={500} data={data} >
      <CartesianGrid/>
      <XAxis dataKey="name" aria-placeholder='chapitre' />
      <YAxis aria-placeholder='moyenne'/>
      <Brush dataKey="name" height={30} stroke="#8884d8" />
      <Bar dataKey="x" fill="white" />
  </BarChart>
);
}
  
