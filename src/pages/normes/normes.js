import React from "react";
import "./normes.css";
import { Link } from "react-router-dom";

export default function Normes() {
  return (
    <div className="Normes">
        
        <div><Link to="/Questions">
            <button type="button" className="iso">
            Conformité ISO 27002
            </button>
            </Link>
        </div>
        <div>
            <button type="button" className="iso">
            Conformité ISO 27003
            </button>
        </div>
        </div>
        
  );
}